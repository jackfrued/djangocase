insert into tb_car (carno, owner, type) values
    ('川A12345', '王大锤', 2),
    ('川A54321', '刘前进', 2),
    ('川B23455', '孙飘飘', 2),
    ('川A12M5B', '李大嘴', 2),
    ('川A1120R', '成都市政', 3),
    ('粤B54099', '石二广', 2),
    ('粤A12345', '马小云', 2);

insert into tb_record (cno, offend_time, offend_place, offend_reason, punish, dealed) values
    (1, '2019-1-15 15:30:25', '科华北路与二环路路口', '不按导向车道行驶', '扣2分罚款100元', 0),
    (1, '2019-2-15 15:30:25', '科华北路与二环路路口', '闯红灯', '扣6分罚款200元', 0),
    (1, '2019-3-15 15:30:25', '科华北路与二环路路口', '超速行驶', '扣3分罚款150元', 0),
    (2, '2019-1-25 21:30:25', '科华北路与二环路路口', '超速20%行驶', '扣3分罚款150元', 0),
    (3, '2019-1-25 20:30:25', '科华北路与二环路路口', '超速20%行驶', '扣3分罚款150元', 0),
    (2, '2019-2-25 19:30:25', '科华北路与二环路路口', '不按导向车道行驶', '扣2分罚款100元', 0),
    (3, '2019-2-25 6:30:25', '科华北路与二环路路口', '不按导向车道行驶', '扣2分罚款100元', 0),
    (4, '2019-3-15 7:30:25', '科华北路与二环路路口', '酒后驾车', '暂扣机动车驾驶证3个月罚款300元', 0),
    (4, '2019-4-15 8:30:25', '科华北路与二环路路口', '超员', '扣2分罚款300元', 0),
    (4, '2019-5-15 9:30:25', '成绵高速德阳段', '车辆故障后不按规定使用灯光和设置警告标志', '扣6分', 0),
    (4, '2019-6-15 10:30:25', '锦绣路', '逆向行驶', '扣2分罚款100元', 0),
    (7, '2019-7-15 11:30:25', '玉林路', '不按规定超车', '扣3分', 0),
    (7, '2019-8-15 12:30:25', '科华北路与二环路路口', '不避让执行任务的警车', '扣3分', 0),
    (7, '2019-9-15 13:30:25', '科华北路与二环路路口', '不按导向车道行驶', '扣2分罚款100元', 0),
    (6, '2019-10-15 14:30:25', '科华北路与二环路路口', '不按导向车道行驶', '扣2分罚款100元', 0),
    (6, '2019-11-15 15:30:25', '科华北路与二环路路口', '超速50%行驶', '吊销机动车驾驶证罚款1000元', 0);
