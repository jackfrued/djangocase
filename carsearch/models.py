from django.db import models


class Car(models.Model):
    """车辆"""

    no = models.AutoField(primary_key=True, verbose_name='编号')
    carno = models.CharField(max_length=10, unique=True, verbose_name='车牌号')
    owner = models.CharField(max_length=20, verbose_name='车主')
    type = models.IntegerField(
        choices=((1, '大型汽车'), (2, '小型汽车'), (3, '专用汽车'), (4, '特种车')),
        default=2, verbose_name='类型'
    )

    def __str__(self):
        return f'{self.carno}'

    class Meta:
        db_table = 'tb_car'
        verbose_name = '车辆'
        verbose_name_plural = '车辆'


class Record(models.Model):
    """违章记录"""

    no = models.AutoField(primary_key=True, verbose_name='编号')
    car = models.ForeignKey(to=Car, on_delete=models.PROTECT, db_constraint=False, db_column='cno', verbose_name='车辆')
    offend_time = models.DateTimeField(verbose_name='违章时间')
    offend_place = models.CharField(max_length=256, verbose_name='违章地点')
    offend_reason = models.CharField(max_length=256, verbose_name='违章原因')
    punish = models.CharField(max_length=256, verbose_name='处罚方式')
    dealed = models.BooleanField(default=False, verbose_name='是否受理')

    class Meta:
        db_table = 'tb_record'
        verbose_name = '违章记录'
        verbose_name_plural = '违章记录'
