from django.contrib import admin

from carsearch.models import Car, Record


class CarModelAdmin(admin.ModelAdmin):
    list_display = ('no', 'carno', 'owner', 'type')
    ordering = ('no', )


class RecordModelAdmin(admin.ModelAdmin):
    list_display = ('no', 'car', 'offend_time', 'offend_place', 'offend_reason', 'punish', 'dealed')
    list_per_page = 5
    ordering = ('no', )


admin.site.register(Car, CarModelAdmin)
admin.site.register(Record, RecordModelAdmin)
